const { MigrationInterface, QueryRunner, Table } = require("typeorm");

module.exports = class CreateTitleRattings1624048050455 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_title_ratings')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_title_ratings',
                columns: [
                    {
                        name: "tconst",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "averageRating",
                        type: "varchar"
                    },
                    {
                        name: "numVotes",
                        type: "varchar"
                    }
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        