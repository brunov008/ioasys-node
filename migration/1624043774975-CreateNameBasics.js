const { MigrationInterface, QueryRunner, Table } = require("typeorm");

module.exports = class CreateNameBasics1624043774975 {

    async up(queryRunner) {

        const hasTable = await queryRunner.hasTable('TB_name_basics')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_name_basics',
                columns: [
                    {
                        name: "nconst",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "primaryName",
                        type: "varchar"
                    },
                    {
                        name: "birthYear",
                        type: "varchar"
                    },
                    {
                        name: "deathYear",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "primaryProfession",
                        type: "varchar"
                    },
                    {
                        name: "knownForTitles",
                        type: "varchar",
                        isNullable: true
                    },
                ]
            }))   
        }
    }

    async down(queryRunner) {
        //await queryRunner.dropTable('TB_name_basics')
    }
}
        