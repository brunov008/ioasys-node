const { MigrationInterface, QueryRunner, Table } = require("typeorm")

module.exports = class CreateTitleVote1624177908347 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_title_vote')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_title_vote',
                columns: [
                    {
                        name: "id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generatedType: "STORED",
                        generationStrategy: "increment"
                    },
                    {
                        name: "tconst",
                        type: "varchar",
                        isNullable: false
                    },
                    {
                        name: "user",
                        type: "int",
                        isNullable: false
                    },
                    {
                        name: "vote",
                        type: "int",
                        isNullable: false
                    }
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        