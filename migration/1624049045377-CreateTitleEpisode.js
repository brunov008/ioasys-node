const { MigrationInterface, QueryRunner, Table } = require("typeorm");

module.exports = class CreateTitleEpisode1624049045377 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_title_episode')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_title_episode',
                columns: [
                    {
                        name: "tconst",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "parentTconst",
                        type: "varchar"
                    },
                    {
                        name: "seasonNumber",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "episodeNumber",
                        type: "varchar",
                        isNullable: true
                    }
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        