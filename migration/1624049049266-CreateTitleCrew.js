const { MigrationInterface, QueryRunner, Table } = require("typeorm")

module.exports = class CreateTitleCrew1624049049266 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_title_crew')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_title_crew',
                columns: [
                    {
                        name: "tconst",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "directors",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "writers",
                        type: "varchar",
                        isNullable: true
                    }
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        