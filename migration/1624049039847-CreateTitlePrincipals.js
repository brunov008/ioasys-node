const { MigrationInterface, QueryRunner, Table } = require("typeorm");

module.exports = class CreateTitlePrincipals1624049039847 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_title_principals') 
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_title_principals',
                columns: [
                    {
                        name: "tconst",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "ordering",
                        type: "varchar"
                    },
                    {
                        name: "nconst",
                        type: "varchar"
                    },
                    {
                        name: "category",
                        type: "varchar"
                    },
                    {
                        name: "job",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "characters",
                        type: "varchar",
                        isNullable: true
                    },
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        