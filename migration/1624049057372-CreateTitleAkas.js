const { MigrationInterface, QueryRunner, Table } = require("typeorm")

module.exports = class CreateTitleAkas1624049057372 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_title_akas')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_title_akas',
                columns: [
                    {
                        name: "titleId",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "ordering",
                        type: "varchar"
                    },
                    {
                        name: "title",
                        type: "varchar"
                    },
                    {
                        name: "region",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "language",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "types",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "attributes",
                        type: "varchar",
                        isNullable: true
                    },
                    {
                        name: "isOriginalTitle",
                        type: "varchar"
                    }
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        