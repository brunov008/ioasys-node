const { MigrationInterface, QueryRunner, Table } = require("typeorm");

module.exports = class CreateTitleBasics1624049053269 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_title_basics')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_title_basics',
                columns: [
                    {
                        name: "tconst",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "titleType",
                        type: "varchar"
                    },
                    {
                        name: "primaryTitle",
                        type: "varchar"
                    },
                    {
                        name: "originalTitle",
                        type: "varchar"
                    },
                    {
                        name: "isAdult",
                        type: "varchar"
                    },
                    {
                        name: "startYear",
                        type: "varchar"
                    },
                    {
                        name: "endYear",
                        type: "varchar"
                    },
                    {
                        name: "runtimeMinutes",
                        type: "varchar"
                    },
                    {
                        name: "genres",
                        type: "varchar"
                    }
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        