const { MigrationInterface, QueryRunner, Table } = require("typeorm")

module.exports = class CreateUser1624127750086 {

    async up(queryRunner) {
        const hasTable = await queryRunner.hasTable('TB_user')
        if(!hasTable){
            await queryRunner.createTable(new Table({
                name: 'TB_user',
                columns: [
                    {
                        name: "id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generatedType: "STORED",
                        generationStrategy: "increment"
                    },
                    {
                        name: "email",
                        type: "varchar",
                        isNullable: false
                    },
                    {
                        name: "senha",
                        type: "varchar",
                        isNullable: false
                    },
                    {
                        name: "excluido",
                        type: "bit",
                        default: 0
                    },
                    {
                        name: "admin",
                        type: "bit",
                        default: 0
                    },
                    {
                        name: "tokenPass",
                        type: "mediumtext",
                        isNullable: true
                    }
                ]
            }))   
        }
    }

    async down(queryRunner) {
    }
}
        