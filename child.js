const {createConnection} = require('typeorm')
const CustomRequest = require('./src/utils/child_script/request')
const {titleRattingsEachLine, titleAkaEachLine, titleBasicsEachLine, titleEpisodeEachLine, nameBasicEachLine, titleCrewEachLine, titlePrincipalsEachLine} = require('./src/utils/child_script/each_line')

const NAME_BASIC_URL = 'https://datasets.imdbws.com/name.basics.tsv.gz'
const TITLE_AKAS_URL = 'https://datasets.imdbws.com/title.akas.tsv.gz'
const TITLE_BASICS_URL = 'https://datasets.imdbws.com/title.basics.tsv.gz'
const TITLE_CREW_URL = 'https://datasets.imdbws.com/title.crew.tsv.gz'
const TITLE_EPISODE_URL = 'https://datasets.imdbws.com/title.episode.tsv.gz'
const TITLE_PRINCIPALS_URL = 'https://datasets.imdbws.com/title.principals.tsv.gz'
const TITLE_RATTINGS_URL = 'https://datasets.imdbws.com/title.ratings.tsv.gz'

createConnection()
.then(connection => {
    new CustomRequest()
        .init(NAME_BASIC_URL, "name.basics.txt", finalPath => nameBasicEachLine(connection, finalPath))
})

createConnection()
.then(connection => {
    new CustomRequest()
        .init(TITLE_AKAS_URL, "title.akas.txt", finalPath => titleAkaEachLine(connection, finalPath))
})

createConnection()
.then(connection => {
    new CustomRequest()
        .init(TITLE_BASICS_URL, "title.basics.txt", finalPath => titleBasicsEachLine(connection, finalPath))
})

createConnection()
.then(connection => {
    new CustomRequest()
        .init(TITLE_EPISODE_URL, "title.episode.txt", finalPath => titleEpisodeEachLine(connection, finalPath))
})

createConnection()
.then(connection => {
    new CustomRequest()
        .init(TITLE_CREW_URL, "title.crew.txt", finalPath => titleCrewEachLine(connection, finalPath))
})

createConnection()
.then(connection => {
    new CustomRequest()
        .init(TITLE_PRINCIPALS_URL, "title.principals.txt", finalPath => titlePrincipalsEachLine(connection, finalPath))
})

createConnection()
.then(connection => {
    new CustomRequest()
        .init(TITLE_RATTINGS_URL, "title.rattings.txt", finalPath => titleRattingsEachLine(connection, finalPath)) 
})