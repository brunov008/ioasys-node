const {CREATED, NOT_FOUND, INTERNAL_SERVER_ERROR, NOT_ACCEPTABLE, BAD_REQUEST} = require('http-status')
const {getConnection} = require('typeorm')

class FilmController{

    getTitleAkasRepository = () => getConnection().getRepository('TitleAkas')
    getTitleVoteRepository = () => getConnection().getRepository('TitleVote')
    getTitleBasicRepository = () => getConnection().getRepository('TitleBasics')
    getTitleEpisodeRepository = () => getConnection().getRepository('TitleEpisode')
    getTitleRattingsRepository = () => getConnection().getRepository('TitleRattings')

    //TODO
    create = async(req, res) => {
        const {title, region, language} = req.body

        await this.getTitleAkasRepository().save(Object.assign({
            titleId: 'bb0000001',
            ordering: '0',
            title: title,
            region : region,
            language: language,
            types: '',
            attributes: '',
            isOriginalTitle: ''
        }))

        res.status(CREATED).json({"message": "ok"})
    }

    //TODO filtro
    list = async(req, res) => {
        const {diretor, nome, genero, atores} = req.query 

        const manager = getConnection().manager

        const query = `SELECT * 
        FROM TB_title_akas as ta
        LEFT JOIN TB_name_basics as nb ON (ta.titleId = nb.nconst)
        LEFT JOIN TB_title_episode as te ON (ta.titleId = te.tconst)
        LEFT JOIN TB_title_crew as tc ON (ta.titleId = tc.tconst)
        LEFT JOIN TB_title_basics as tb ON(ta.titleId = tb.tconst)
        LEFT JOIN TB_title_principals as tp ON (ta.titleId = tp.tconst)`

        /*
            WHERE ta.title LIKE 'Sar%' -- nome do filme
            OR tp.characters LIKE '' -- atores
            OR tb.genres LIKE '' -- generos
            OR tc.directors LIKE ''; -- diretores
            LIMIT ... OFFSET ...
        */

        const result = await manager.query(query)

        const list = []

        res.json(list.concat(...result).reverse())
    }

    getOne = async(req, res) => {
        const {idFilm} = req.params

        const manager = getConnection().manager

        const film = await manager.query(`SELECT * 
        FROM TB_title_akas as ta
        LEFT JOIN TB_name_basics as nb ON (ta.titleId = nb.nconst)
        LEFT JOIN TB_title_episode as te ON (ta.titleId = te.tconst)
        LEFT JOIN TB_title_crew as tc ON (ta.titleId = tc.tconst)
        LEFT JOIN TB_title_basics as tb ON(ta.titleId = tb.tconst)
        LEFT JOIN TB_title_principals as tp ON (ta.titleId = tp.tconst)
        LEFT JOIN TB_title_ratings as tr ON (ta.titleId = tr.tconst)
        WHERE ta.titleId = "${idFilm}"`) //Possible Inject

        if(!film[0]) return res.status(NOT_FOUND).json({"message": "Filme não existe."})

        res.json(film[0])
    }

    vote = async(req, res) => {
        const {idFilm} = req.params
        const {vote} = req.body
        const number = [0,1,2,3,4]

        if(req.isADM !== undefined) {
            return res.status(NOT_ACCEPTABLE).json({
                message: 'Apenas usuários que não sejam administradoras podem votar'
            }) 
        }

        if(!number.some(value => value === vote)){
            return res.status(BAD_REQUEST).json({
                message: 'voto deve ser de 0-4'
            }) 
        }

        const film = await this.getTitleAkasRepository().findOne({
            where: { 
                titleId : String(idFilm)
            }
        })

        if(!film) return res.status(NOT_FOUND).json({"message": "Filme não existe."})

        await this.getTitleVoteRepository().save(Object.assign({
            tconst: String(film.titleId),
            user: Number(req.decoded.id),
            vote: Number(vote)
        }))

        const ratting = await this.getTitleRattingsRepository().findOne({
            where: {
                tconst: String(film.titleId)
            }
        })

        ratting.numVotes = Number(ratting.numVotes) + 1

        await this.getTitleRattingsRepository().save(ratting)

        res.json({"message": "ok"})
    }
}

module.exports = new FilmController()