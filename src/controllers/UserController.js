const {getConnection} = require('typeorm')
const {CREATED, NOT_FOUND, INTERNAL_SERVER_ERROR, BAD_REQUEST} = require('http-status')
const {encrypt, decrypt} = require('../utils/aes256')
const {parseBufferToInt} = require('../utils/utility')
const Auth = require('../utils/auth')

class UserController{

    getUserRepository = () => getConnection().getRepository('User')

    authenticate = async (req, res) => {
        const {email, senha} = req.body

        const user = await this.getUserRepository().findOne({
            where: { 
                email : email,
                excluido: 0
            }
        })
        
        if(!user || senha !== decrypt(user.senha.trim())) return res.status(NOT_FOUND).json({"message": "Usuario não existe."})

        delete user.senha
        delete user.tokenPass
        user.excluido = parseBufferToInt(user.excluido)
        user.admin = parseBufferToInt(user.admin)

        const accessToken = Auth.token(user) 

        delete user.excluido
        delete user.admin

        const token = Object.assign({
            accessToken,
            tokenType: 'Bearer'
        })

        res.json({
            token,
            user
        })
    }
    
    create = async (req, res) => {
        const {email, senha} = req.body

        await  this.getUserRepository().save(Object.assign({
            email: email.trim(),
            senha: encrypt(senha.trim()),
            admin: req.isADM ? 1 : 0
        }))

        res.status(CREATED).json({"message": "ok"})
    }

    update = async (req, res) => {
        const {id} = req.params
        const {email, senha} = req.body

        const user = await this.getUserRepository().findOne({
            where: { 
                id : id
            }
        })

        if(!user) return res.status(NOT_FOUND).json({"message": "Usuario não existe."})

        user.email = email.trim()
        user.senha = encrypt(senha.trim())

        await this.getUserRepository().save(user)

        res.json({"message": "ok"})
    }

    deleteById = async (req, res) => {
        const {id} = req.params
        const {status} = req.query

        if(status && ![0,1].some(value => value === Number(status))){
            return res.status(BAD_REQUEST).json({
                message: 'status deve ser 1 ou 0'
            }) 
        }

        const user = await this.getUserRepository().findOne({
            where: { 
                id : id
            }
        })

        if(!user) return res.status(404).json({"message": "Usuario não existe."})

        user.excluido = Number(status) ? Number(status) : 1

        await this.getUserRepository().save(user) 

        res.json({"message": "ok"})
    }
}

module.exports = new UserController()