const express = require('express')
const app = express()
const cors = require('cors')
const routes = require('./routes')
const helmet = require('helmet')
const ScriptQueue = require('./script')
const {createConnection} = require('typeorm')

class Server{
    constructor(){
        if (process.env.NODE_ENV !== 'production') require('dotenv').config()

       createConnection()
       .then(async c => {

            console.log('Conexao com o banco realizada com sucesso')

            await c.runMigrations({transaction: 'all'})

            app.use(cors({
                "origin": "*",
                "methods": "GET,PUT,POST,DELETE",
                "allowedHeaders": ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization', 'Host', 'User-Agent'],
                "preflightContinue": false
            }))
            
            app.use(helmet())

            app.use(express.json())

            app.use('/', routes)

            app.listen(
                Number(process.env.PORT),
                String(process.env.HOST_NAME), () => {
                    if(String(process.env.START_SCRIPT) === 'true') new ScriptQueue()
                    console.log("Aplicação rodando na porta ",process.env.PORT)
                })
       })
       .catch(reason => console.log(reason))
    }
}

module.exports = Server