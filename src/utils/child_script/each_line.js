const {createInterface} = require('readline')
const {createReadStream} = require('fs')

const regex = /([^\t]+)/g
const MAX_LINES = 100 //FIXME More than 10000 Is Causing Nodejs Memory Heap [BUG] - too many lines to save in database x.x

const nameBasicEachLine = async (connection, finalPath) => {
    let qtdNameBasicLinesRead = 0

    const rl = createInterface({
        input: createReadStream(finalPath, {encoding: "utf-8"}),
        //crlfDelay: Infinity
    })

    for await (const line of rl) {
        if(qtdNameBasicLinesRead === MAX_LINES){
            process.send({onSuccess : '---Dados inseridos no banco com sucesso---'})
            break;
        }
    
        const match = String(line).match(regex)
    
        connection.getRepository('NameBasics').save(Object.assign({
            nconst: match[0],
            primaryName: `${match[1]} ${match[2]}`, //FIXME primaryName pode ter 3 [BUG]
            birthYear: match[3],
            deathYear: match[4].startsWith('\N') || match[4].startsWith('\\N') ? null : match[4],
            primaryProfession: match[5],
            knownForTitles: match[6]
        }))
        .catch(err => process.send({onError : `Erro ao adicionar arquivo no banco. Reason: ${err}`}))

        qtdNameBasicLinesRead++
    }
    rl.pause()
    rl.close()
}

const titleAkaEachLine = async (connection, finalPath) => {
    let qtdTitleAkaLinesRead = 0

    const rl = createInterface({
        input: createReadStream(finalPath, {encoding: "utf-8"}),
        //crlfDelay: Infinity
    })

    for await (const line of rl) {
        if(qtdTitleAkaLinesRead === MAX_LINES){
            process.send({onSuccess : '---Dados inseridos no banco com sucesso---'})
            break;
        }
    
        const match = String(line).match(regex)
    
       connection.getRepository('TitleAkas').save(Object.assign({
            titleId: match[0],
            ordering: match[1],
            title: match[2],
            region: match[3].startsWith('\N') || match[3].startsWith('\\N') ? null : match[3],
            language: match[4].startsWith('\N') || match[4].startsWith('\\N') ? null : match[4],
            types: match[5].startsWith('\N') || match[5].startsWith('\\N') ? null : match[5],
            attributes: match[6].startsWith('\N') || match[6].startsWith('\\N') ? null : match[6],
            isOriginalTitle: match[7]
        }))
        .catch(err => process.send({onError : `Erro ao adicionar arquivo no banco. Reason: ${err}`}))

        qtdTitleAkaLinesRead++
    }
    rl.pause()
    rl.close()
}

const titleBasicsEachLine = async (connection, finalPath) => {
    let qtdTitleBasicLinesRead = 0

    const rl = createInterface({
        input: createReadStream(finalPath, {encoding: "utf-8"}),
        //crlfDelay: Infinity
    })

    for await (const line of rl) {
        if(qtdTitleBasicLinesRead === MAX_LINES){
            process.send({onSuccess : '---Dados inseridos no banco com sucesso---'})
            break;
        }
    
        const match = String(line).match(regex)
    
        connection.getRepository('TitleBasics').save(Object.assign({
            tconst: match[0],
            titleType: match[1],
            primaryTitle: match[2],
            originalTitle: match[3],
            isAdult: match[4],
            startYear: match[5],
            endYear: match[6],
            runtimeMinutes: match[7],
            genres: match[8]
        }))
        .catch(err => process.send({onError : `Erro ao adicionar arquivo no banco. Reason: ${err}`}))

        qtdTitleBasicLinesRead++
    }
    rl.pause()
    rl.close()
} 

const titleEpisodeEachLine = async (connection, finalPath) => {
    let qtdTitleEpisodeLinesRead = 0

    const rl = createInterface({
        input: createReadStream(finalPath, {encoding: "utf-8"}),
        //crlfDelay: Infinity
    })

    for await (const line of rl) {
        if(qtdTitleEpisodeLinesRead === MAX_LINES){
            process.send({onSuccess : '---Dados inseridos no banco com sucesso---'})
            break;
        }
    
        const match = String(line).match(regex)
    
        connection.getRepository('TitleEpisode').save(Object.assign({
            tconst: match[0],
            parentTconst: match[1],
            seasonNumber: match[2].startsWith('\N') || match[2].startsWith('\\N') ? null : match[2],
            episodeNumber: match[3].startsWith('\N') || match[3].startsWith('\\N') ? null : match[3]
        }))
        .catch(err => process.send({onError : `Erro ao adicionar arquivo no banco. Reason: ${err}`}))

        qtdTitleEpisodeLinesRead++
    }
    rl.pause()
    rl.close()
}

const titleCrewEachLine = async (connection, finalPath) => {
    let qtdTitleCrewLinesRead = 0

    const rl = createInterface({
        input: createReadStream(finalPath, {encoding: "utf-8"}),
        //crlfDelay: Infinity
    })

    for await (const line of rl) {
        if(qtdTitleCrewLinesRead === MAX_LINES){
            process.send({onSuccess : '---Dados inseridos no banco com sucesso---'})
            break;
        }
    
        const match = String(line).match(regex)
    
        connection.getRepository('TitleCrew').save(Object.assign({
            tconst: match[0],
            directors: match[1].startsWith('\N') || match[1].startsWith('\\N') ? null : match[1],
            writers: match[2].startsWith('\N') || match[2].startsWith('\\N') ? null : match[2],
        }))
        .catch(err => process.send({onError : `Erro ao adicionar arquivo no banco. Reason: ${err}`}))

        qtdTitleCrewLinesRead++
    }
    rl.pause()
    rl.close()
}

const titlePrincipalsEachLine = async (connection, finalPath) => {
    let qtdTitlePrincipalsLinesRead = 0

    const rl = createInterface({
        input: createReadStream(finalPath, {encoding: "utf-8"}),
        //crlfDelay: Infinity
    })

    for await (const line of rl) {
        if(qtdTitlePrincipalsLinesRead === MAX_LINES){
            process.send({onSuccess : '---Dados inseridos no banco com sucesso---'})
            break;
        }
    
        const match = String(line).match(regex)
    
        connection.getRepository('TitlePrincipals').save(Object.assign({
            tconst: match[0],
            ordering: match[1],
            nconst: match[2],
            category: match[3],
            job: match[4].startsWith('\N') || match[4].startsWith('\\N') ? null : match[4],
            characters: match[5].startsWith('\N') || match[5].startsWith('\\N') ? null : match[5]
        }))
        .catch(err => process.send({onError : `Erro ao adicionar arquivo no banco. Reason: ${err}`}))

        qtdTitlePrincipalsLinesRead++
    }
    rl.pause()
    rl.close()  
}

const titleRattingsEachLine = async (connection, finalPath) => {
    let qtdTitleRattingsLinesRead = 0

    const rl = createInterface({
        input: createReadStream(finalPath, {encoding: "utf-8"}),
        //crlfDelay: Infinity
    })

    for await (const line of rl) {
        if(qtdTitleRattingsLinesRead === MAX_LINES){
            process.send({onSuccess : '---Dados inseridos no banco com sucesso---'})
            break;
        }
    
        const match = String(line).match(regex)
    
        connection.getRepository('TitleRattings').save(Object.assign({
            tconst: match[0],
            averageRating: match[1],
            numVotes: match[2]
        }))
        .catch(err => process.send({onError : `Erro ao adicionar arquivo no banco. Reason: ${err}`}))

        qtdTitleRattingsLinesRead++
    }
    rl.pause()
    rl.close()  
}

module.exports = {titleRattingsEachLine, titlePrincipalsEachLine, nameBasicEachLine, titleAkaEachLine, titleBasicsEachLine, titleEpisodeEachLine, titleCrewEachLine}