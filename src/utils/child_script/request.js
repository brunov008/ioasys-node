const {get} = require('https')
const {createGunzip} = require('zlib')
const {existsSync, mkdirSync, createWriteStream, createReadStream, unlinkSync} = require('fs')
const {resolve} = require ('path')
const dir = resolve(__dirname, '..', '..','..', 'data')

class CustomRequest{
    
    init(endpoint, fileName, callback){
        get(endpoint, res => {

            if(res.statusCode <= 199 && res.statusCode >= 300){
                process.send({onError : `Erro ao buscar os dados no endpoint ${endpoint}`})
                return;
            }

            if (!existsSync(dir)) mkdirSync(dir)

            process.send({onSuccess : `---GET 200 ${endpoint}---`})

            const finalPath = resolve(dir, fileName)

            if(existsSync(finalPath)){
                unlinkSync(finalPath)
            }

            process.send({onSuccess : `---Unzip ${fileName}---`})

            const stream = createWriteStream(finalPath)
            const gunzip = createGunzip()
            res.pipe(gunzip)
            gunzip.on('data', data => stream.write(data, "utf8"))
            gunzip.on('error', err => process.send({onError: err}))
            gunzip.on('end', () => {
                gunzip.close()
                stream.close()

                callback(finalPath)
            })
        })
        .on('error', () => process.send({onError : `Erro ao buscar os dados no endpoint ${endpoint}. Tente se conectar novamente`}))
    }
}

module.exports = CustomRequest