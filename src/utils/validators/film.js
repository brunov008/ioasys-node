const { body , param, query} = require('express-validator')

const listValidator = [
    query('diretor')
        .isString()
        .not().isEmpty().withMessage('Campo vazio')
        .not().contains('"')
        .not().contains("'"),

    query('nome')
        .isString()
        .not().isEmpty().withMessage('Campo vazio')
        .not().contains('"')
        .not().contains("'"),

    query('genero')
        .isString()
        .not().isEmpty().withMessage('Campo vazio')
        .not().contains('"')
        .not().contains("'"),

    query('atores')
        .isString()
        .not().isEmpty().withMessage('Campo vazio')
        .not().contains('"')
        .not().contains("'"),
]

const getOneValidator = [
    param('idFilm')
        .exists().withMessage('Necessario id filme')
        .not().isEmpty().withMessage('Campo vazio')
        .not().contains('"')
        .not().contains("'"),
]

const voteValidator = [
    param('idFilm')
        .exists().withMessage('Necessario id filme')
        .not().isEmpty().withMessage('Campo vazio')
        .not().contains('"')
        .not().contains("'"),
    body('vote')
        .exists().withMessage('Necessario voto de 0 a 4')
        .isNumeric({
            no_symbols: true
    })
]

module.exports = { getOneValidator, listValidator, voteValidator}