const { body , param} = require('express-validator')

const createValidator = [
    body('email')
        .exists().withMessage('É necessário email') 
        .not().isEmpty().withMessage('Campo vazio')
        .isString()
        .isEmail(),
        
    body('senha')
        .exists().withMessage('É necessário senha') 
        .not().isEmpty().withMessage('Campo vazio')
        .isString(),
]

const putValidator = [
    param('id')
        .exists().withMessage('Necessário id')
        .not().isEmpty().withMessage('Campo vazio')
        .isNumeric({
            no_symbols: true
        }),

    body('email')
        .exists().withMessage('É necessário email') 
        .not().isEmpty().withMessage('Campo vazio')
        .isString()
        .isEmail(),
        
    body('senha')
        .exists().withMessage('É necessário senha') 
        .not().isEmpty().withMessage('Campo vazio')
        .isString(),
]

const authenticateValidator = [
    body('email')
        .exists().withMessage('É necessário email') 
        .not().isEmpty().withMessage('Campo vazio')
        .isString()
        .isEmail(),
        
    body('senha')
        .exists().withMessage('É necessário senha') 
        .not().isEmpty().withMessage('Campo vazio')
        .isString(),
]

const deleteValidator = [
    param('id')
        .exists().withMessage('Necessario id')
        .not().isEmpty().withMessage('Campo vazio')
        .isNumeric({
            no_symbols: true
        }),
]

module.exports = { createValidator, putValidator, deleteValidator, authenticateValidator}