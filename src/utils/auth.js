const {encode, decode} = require('jwt-simple')
const moment = require ('moment-timezone')
const jwtSecret = require('./_')

class Auth{

    static isValidToken(token){
        let response = {
            isValid : false,
            user: null
        }

        try{
            if(token === null){
                response.isValid = false
            }else{
                if(String(token).startsWith('Bearer')){
                    let decoded = decode(String(token).replace('Bearer ', ''), jwtSecret)

                    if(decoded){
                        response.user = decoded.sub
                        response.isValid = true
                    }else{
                        response.isValid = false
                    }
                }else{
                    response.isValid = false
                }
            }
        }catch(e){
            response.isValid = false
        }

        return response
    }

    static token(user) {
        const payload = {
            exp: moment().add(1440, 'minutes').unix(),
            iat: moment().unix(),
            sub: user
        }

        return encode(payload, jwtSecret)
    }

}

module.exports = Auth