const Auth = require('../../utils/auth')
const {UNAUTHORIZED} = require('http-status')

const tokenMiddleware = (req, res, next) => {
    let token = req.headers['authorization'] || req.headers['Authorization']

    const {isValid, user} = Auth.isValidToken(token)

    if(!isValid) {
        return res.status(UNAUTHORIZED).json({
            message: 'Token invalido'
        })
    }

    req.decoded = user
    next()
}

const onlyAdmin = (req, res, next) => {
    const user = req.decoded
    if(user.admin){
        req.isADM = true
        next()
    }else{
        return res.status(UNAUTHORIZED).json({
            message: 'Apenas pessoas autorizadas'
        }) 
    }
}

module.exports = {tokenMiddleware, onlyAdmin}