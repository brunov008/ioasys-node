const {UNPROCESSABLE_ENTITY} = require('http-status')
const { validationResult } = require('express-validator');

const validate = (req, res, next) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
      next()
    }else{
      //FIXME retorno das mensagens :(
      return res.status(UNPROCESSABLE_ENTITY).json({errors: errors.array()})
    }
}

module.exports = validate