const uRouter = require('express').Router() 
const {create, update, deleteById} = require('../../controllers/UserController')
const {tokenMiddleware, onlyAdmin} = require('../middlewares/auth')
const validate = require('../middlewares/validation')
const {createValidator, putValidator, deleteValidator} = require('../../utils/validators/user')


uRouter.post('/', createValidator, validate, create)
uRouter.post('/admin', createValidator, validate, tokenMiddleware, onlyAdmin, create)
uRouter.put('/:id', putValidator, validate, tokenMiddleware, update)
uRouter.delete('/:id', deleteValidator, validate, tokenMiddleware, deleteById)

module.exports = uRouter