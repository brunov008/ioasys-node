const fRouter = require('express').Router() 
const {create, list, getOne, vote} = require('../../controllers/FilmController')
const {onlyAdmin} = require('../middlewares/auth')
const validate = require('../middlewares/validation')
const {getOneValidator, listValidator, voteValidator} = require('../../utils/validators/film')

fRouter.post('/', onlyAdmin, create)
fRouter.get('/', listValidator, list)
fRouter.get('/:idFilm', getOneValidator, validate, getOne)
fRouter.post('/:idFilm/vote', voteValidator, validate, vote)

module.exports = fRouter
