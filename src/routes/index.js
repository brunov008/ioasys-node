const router = require('express').Router()
const userRoutes = require('./users') 
const filmRoutes = require('./films')
const {authenticate} = require('../controllers/UserController')
const {tokenMiddleware} = require('./middlewares/auth')
const validate = require('./middlewares/validation')
const {authenticateValidator} = require('../utils/validators/user')

router.post('/autenticar', authenticateValidator, validate, authenticate)
router.use('/users', userRoutes)
router.use('/films',tokenMiddleware, filmRoutes)

module.exports = router