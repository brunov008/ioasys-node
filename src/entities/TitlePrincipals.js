const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name": "TitlePrincipals", 
   "tableName": "TB_title_principals", 
   "columns": { 
      "tconst": { 
         "primary": true,  
         "type": "varchar"  
      }, 
      "ordering": { 
         "type": "varchar" 
      }, 
      "nconst": { 
         "type": "varchar" 
      },
      "category": { 
         "type": "varchar" 
      },
      "job": { 
         "type": "varchar",
         "nullable": true 
      },
      "characters": { 
         "type": "varchar",
         "nullable": true 
      }
   } 
})