const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name": "User", 
   "tableName": "TB_user", 
   "columns": { 
      "id": { 
         "primary": true,  
         "type": "int",
         "generated": true
      }, 
      "email": { 
         "type": "varchar",
         "nullable": false
      }, 
      "senha": { 
         "type": "varchar",
         "nullable": false
      },
      "excluido": { 
         "type": "bit",
         "nullable": true,
         "default": 0
      },
      "admin": { 
         "type": "bit",
         "nullable": true,
         "default": 0
      },
      "tokenPass": { 
         "type": "mediumtext",
         "nullable":true
      }
   } 
})