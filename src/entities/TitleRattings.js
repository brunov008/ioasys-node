const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name": "TitleRattings", 
   "tableName": "TB_title_ratings", 
   "columns": { 
      "tconst": { 
         "primary": true,  
         "type": "varchar"
      }, 
      "averageRating": { 
         "type": "varchar" 
      }, 
      "numVotes": { 
         "type": "varchar" 
      }
   } 
})