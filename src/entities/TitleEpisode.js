const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name":"TitleEpisode",
   "tableName": "TB_title_episode", 
   "columns": { 
      "tconst": { 
         "primary": true,  
         "type": "varchar"
      }, 
      "parentTconst": { 
         "type": "varchar" 
      }, 
      "seasonNumber": { 
         "type": "varchar",
         "nullable": true  
      },
      "episodeNumber": { 
         "type": "varchar",
         "nullable": true    
      }
   } 
})