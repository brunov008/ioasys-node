const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name": "TitleVote", 
   "tableName": "TB_title_vote", 
   "columns": { 
      "id": { 
         "primary": true,  
         "type": "int",
         "generated": true
      }, 
      "tconst": { 
         "type": "varchar",
         "nullable": false
      }, 
      "user": { 
         "type": "int",
         "nullable": false 
      },
      "vote": {
         "type": "int",
         "nullable": false 
      }
   } 
})