const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name":"TitleCrew",
   "tableName": "TB_title_crew", 
   "columns": { 
      "tconst": { 
         "primary": true,  
         "type": "varchar"
      }, 
      "directors": { 
         "type": "varchar",
         "nullable": true 
      }, 
      "writers": { 
         "type": "varchar",
         "nullable": true 
      }
   } 
})