const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name": "NameBasics", 
   "tableName": "TB_name_basics", 
   "columns": { 
      "nconst": { 
         "primary": true,  
         "type": "varchar"
      }, 
      "primaryName": { 
         "type": "varchar" 
      }, 
      "birthYear": { 
         "type": "varchar" 
      },
      "deathYear": { 
         "type": "varchar",
         "nullable": true
      },
      "primaryProfession": { 
         "type": "varchar" 
      },
      "knownForTitles": { 
         "type": "varchar",
         "nullable":true
      }
   } 
})