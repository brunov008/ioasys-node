const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
   "name": "TitleBasics",
   "tableName": "TB_title_basics", 
   "columns": { 
      "tconst": { 
         "primary": true,  
         "type": "varchar"
      }, 
      "titleType": { 
         "type": "varchar" 
      }, 
      "primaryTitle": { 
         "type": "varchar" 
      },
      "originalTitle": { 
         "type": "varchar" 
      },
      "isAdult": { 
         "type": "varchar" 
      },
      "startYear": { 
         "type": "varchar" 
      },
      "endYear": { 
         "type": "varchar" 
       },
       "runtimeMinutes": { 
         "type": "varchar" 
       },
       "genres": { 
         "type": "varchar" 
       }
   } 
})