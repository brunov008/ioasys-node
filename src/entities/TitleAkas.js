const {EntitySchema} = require('typeorm')

module.exports = new EntitySchema({ 
    "name": "TitleAkas", 
    "tableName": "TB_title_akas", 
    "columns": { 
       "titleId": { 
          "primary": true,  
          "type": "varchar"
       }, 
       "ordering": { 
          "type": "varchar" 
       }, 
       "title": { 
          "type": "varchar" 
       },
       "region": { 
          "type": "varchar",
          "nullable": true 
       },
       "language": { 
          "type": "varchar",
          "nullable": true 
       },
       "types": { 
          "type": "varchar",
          "nullable": true 
       },
       "attributes": { 
          "type": "varchar" ,
          "nullable": true
        },
        "isOriginalTitle": { 
            "type": "varchar" 
        }
    } 
 })