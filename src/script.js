const child_process  = require('child_process')
const {resolve} = require ('path')

class ScriptQueue{
    constructor(){
        this.child = child_process.fork(resolve(__dirname, '..', 'child.js'))

        this.child.on('message', result => {
            if(result.onError != null){
                console.log(result.onError)
            }else if(result.onSuccess != null){
                console.log(result.onSuccess)
            }
        })
    }
}

module.exports = ScriptQueue